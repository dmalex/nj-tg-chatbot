# How to run it

First, get a bot token from the [@BotFather](http://telegram.me/botfather). Once you got that, clone the repo, create a `.env`
file with the same format as `.env.example` and put the token it gives you there. Then:

```
BOT_TOKEN must be real token from @BotFather
npm install
node .
```

And your bot should start answering through Telegram.

https://digitrain.ru/articles/124883/
